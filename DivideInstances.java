package net.aprendizajengrande.ontocluster;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Reader;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.apache.mahout.clustering.Cluster;
import org.apache.mahout.clustering.iterator.ClusterWritable;
import org.apache.mahout.clustering.kmeans.KMeansDriver;
import org.apache.mahout.clustering.kmeans.RandomSeedGenerator;
import org.apache.mahout.common.Pair;
import org.apache.mahout.common.distance.CosineDistanceMeasure;
import org.apache.mahout.common.distance.DistanceMeasure;
import org.apache.mahout.common.iterator.sequencefile.PathType;
import org.apache.mahout.common.iterator.sequencefile.SequenceFileDirIterable;
import org.apache.mahout.math.VectorWritable;
import org.apache.mahout.math.Vector;

import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.io.compress.CompressionCodec;
import java.io.InputStream;
import java.io.StringWriter;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.fs.FileStatus;
import java.util.ArrayList;
import java.util.List;
import java.lang.Integer;

public class DivideInstances {

    public static List<String> readLines(Path location, Configuration conf) throws Exception {
        FileSystem fileSystem = FileSystem.get(location.toUri(), conf);
        CompressionCodecFactory factory = new CompressionCodecFactory(conf);
        FileStatus[] items = fileSystem.listStatus(location);
        if (items == null) return new ArrayList<String>();
        List<String> results = new ArrayList<String>();
        for(FileStatus item: items) {

          // ignoring files like _SUCCESS
          if(item.getPath().getName().startsWith("_")) {
            continue;
          }

          CompressionCodec codec = factory.getCodec(item.getPath());
          InputStream stream = null;

          // check if we have a compression codec we need to use
          if (codec != null) {
            stream = codec.createInputStream(fileSystem.open(item.getPath()));
          }
          else {
            stream = fileSystem.open(item.getPath());
          }

          StringWriter writer = new StringWriter();
          IOUtils.copy(stream, writer, "UTF-8");
          String raw = writer.toString();
          String[] resulting = raw.split("\n");
          for(String str: raw.split("\n")) {
            results.add(str);
          }
        }
        return results;
      }



	public static void main(String[] args) throws ClassNotFoundException,
			IOException, InterruptedException, Exception {

		if (args.length != 2) {
			System.err
					.println("Usage: <hdfs input folder> <hdfs ouput folder>");
			System.exit(1);
		}

		Path inputdir = new Path(args[0]);
		Path input = new Path(args[0] + "/input");
		Path randpath = new Path(args[0] + "/randset");
		Path set1path = new Path(args[1] + "/set1");
		Path set2path = new Path(args[1] + "/set2");

		Configuration conf = new Configuration();
		conf.set("fs.hdfs.impl",
				org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
		conf.set("fs.file.impl",
				org.apache.hadoop.fs.LocalFileSystem.class.getName());

		FileSystem fs = inputdir.getFileSystem(conf);
		SequenceFile.Reader reader = new SequenceFile.Reader(fs, input, conf);

		SequenceFile.Writer writer1 = SequenceFile.createWriter(conf,
				Writer.file(set1path), Writer.keyClass(Text.class),
				Writer.valueClass(VectorWritable.class));
		SequenceFile.Writer writer2 = SequenceFile.createWriter(conf,
				Writer.file(set2path), Writer.keyClass(Text.class),
				Writer.valueClass(VectorWritable.class));

		List<String> randsets = readLines(randpath, conf);

		Text key = new Text();
		VectorWritable vector = new VectorWritable();
		Integer j = new Integer(0);
		while (reader.next(key, vector)) {
			Integer set = Integer.parseInt(randsets.get(j));
			if(set == 0) writer1.append(key, vector);
			else writer2.append(key, vector);
			j += 1;
		}
		writer1.close();
		writer2.close();
		reader.close();
	}
}
