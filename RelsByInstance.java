package net.aprendizajengrande.ontocluster;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Reader;
import org.apache.mahout.clustering.Cluster;
import org.apache.mahout.clustering.iterator.ClusterWritable;
import org.apache.mahout.clustering.kmeans.KMeansDriver;
import org.apache.mahout.clustering.kmeans.RandomSeedGenerator;
import org.apache.mahout.common.Pair;
import org.apache.mahout.common.distance.CosineDistanceMeasure;
import org.apache.mahout.common.distance.DistanceMeasure;
import org.apache.mahout.common.iterator.sequencefile.PathType;
import org.apache.mahout.common.iterator.sequencefile.SequenceFileDirIterable;
import org.apache.mahout.math.VectorWritable;
import org.apache.mahout.math.Vector;

public class RelsByInstance {

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, InterruptedException {

		if (args.length != 2) {
			System.err
					.println("Usage: <hdfs input folder> <local ouput file>");
			System.exit(1);
		}

		Path inputdir = new Path(args[0]);
		Path input = new Path(args[0] + "/input");

		Configuration conf = new Configuration();
		conf.set("fs.hdfs.impl",
				org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
		conf.set("fs.file.impl",
				org.apache.hadoop.fs.LocalFileSystem.class.getName());

		FileSystem fs = inputdir.getFileSystem(conf);
		SequenceFile.Reader reader = new SequenceFile.Reader(fs, input, conf);
		PrintWriter pw = new PrintWriter(new FileWriter(new File(args[1])));

		Text key = new Text();
		VectorWritable obj = new VectorWritable();
		while (reader.next(key, obj)) {
			if (!(obj instanceof VectorWritable))
				continue;
			Vector vector = ((VectorWritable) obj).get();
			for (int i = 0; i < vector.size(); i++) {
				if (vector.get(i) > 0.1) {
					pw.print(i);
					pw.print(" ");
				}
			}
			pw.print("-1");
			pw.println();
		}
		pw.close();
	}
}
