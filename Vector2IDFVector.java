package net.aprendizajengrande.ontocluster;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Reader;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.apache.mahout.clustering.Cluster;
import org.apache.mahout.clustering.iterator.ClusterWritable;
import org.apache.mahout.clustering.kmeans.KMeansDriver;
import org.apache.mahout.clustering.kmeans.RandomSeedGenerator;
import org.apache.mahout.common.Pair;
import org.apache.mahout.common.distance.CosineDistanceMeasure;
import org.apache.mahout.common.distance.DistanceMeasure;
import org.apache.mahout.common.iterator.sequencefile.PathType;
import org.apache.mahout.common.iterator.sequencefile.SequenceFileDirIterable;
import org.apache.mahout.math.VectorWritable;
import org.apache.mahout.math.Vector;
import org.apache.mahout.math.SequentialAccessSparseVector;

import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.io.compress.CompressionCodec;
import java.io.InputStream;
import java.io.StringWriter;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.fs.FileStatus;
import java.util.ArrayList;
import java.util.List;
import java.lang.Integer;

public class Vector2IDFVector {

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, InterruptedException, Exception {

		if (args.length != 3) {
			System.err
					.println("Usage: <hdfs input folder> <hdfs IDFs file> <hdfs output folder>");
			System.exit(1);
		}

		Path inputdir = new Path(args[0]);
		Path input = new Path(args[0] + "/input");
		Path idfpath = new Path(args[1]);
		Path output = new Path(args[2] + "/input");


		Configuration conf = new Configuration();
		conf.set("fs.hdfs.impl",
				org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
		conf.set("fs.file.impl",
				org.apache.hadoop.fs.LocalFileSystem.class.getName());

		FileSystem fs = inputdir.getFileSystem(conf);
		SequenceFile.Reader reader = new SequenceFile.Reader(fs, input, conf);

		SequenceFile.Writer writer = SequenceFile.createWriter(conf,
				Writer.file(output), Writer.keyClass(Text.class),
				Writer.valueClass(VectorWritable.class));


		List<String> idfs = DivideInstances.readLines(idfpath, conf);

		Text key = new Text();
		VectorWritable vectorw = new VectorWritable();
		while (reader.next(key, vectorw)) {
			Vector vector = vectorw.get();
			Vector newv = new SequentialAccessSparseVector(1099);
			for(int j=0; j<1099; j++) {
				newv.set(j, vector.get(j)*Double.parseDouble(idfs.get(j)));
			}
			writer.append(new Text(key), new VectorWritable(newv));
		}
		writer.close();
		reader.close();
	}
}
