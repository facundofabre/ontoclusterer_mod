#include <iostream>
#include <cmath>

#define NRELS 1099

using namespace std;

int main(void) {
    int idx = -1;
    int freq[NRELS];
    for(int i=0; i<NRELS; i++)
        freq[i] = 0;
    int numdocs = 0;
    while(cin >> idx) {
        if(idx == -1)
            numdocs++;
        else
            freq[idx]++;
    }
    // Adding 1 to freq[i] cause there are relations with freq 0
    for(int i=0; i<NRELS; i++)
        cout << sqrt(log((double)numdocs/(freq[i]+1.)) + 1.) << endl;
}
