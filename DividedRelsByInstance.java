package net.aprendizajengrande.ontocluster;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Reader;
import org.apache.mahout.clustering.Cluster;
import org.apache.mahout.clustering.iterator.ClusterWritable;
import org.apache.mahout.clustering.kmeans.KMeansDriver;
import org.apache.mahout.clustering.kmeans.RandomSeedGenerator;
import org.apache.mahout.common.Pair;
import org.apache.mahout.common.distance.CosineDistanceMeasure;
import org.apache.mahout.common.distance.DistanceMeasure;
import org.apache.mahout.common.iterator.sequencefile.PathType;
import org.apache.mahout.common.iterator.sequencefile.SequenceFileDirIterable;
import org.apache.mahout.math.VectorWritable;
import org.apache.mahout.math.Vector;

import org.apache.hadoop.io.compress.CompressionCodecFactory;
import org.apache.hadoop.io.compress.CompressionCodec;
import java.io.InputStream;
import java.io.StringWriter;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.fs.FileStatus;
import java.util.ArrayList;
import java.util.List;
import java.lang.Integer;


public class DividedRelsByInstance {

	public static void main(String[] args) throws ClassNotFoundException,
			IOException, InterruptedException, Exception {

		if (args.length != 2) {
			System.err
					.println("Usage: <hdfs input folder> <local ouput folder>");
			System.exit(1);
		}

		Path inputdir = new Path(args[0]);
		Path input = new Path(args[0] + "/input");
		Path randpath = new Path(args[0] + "/randset");

		Configuration conf = new Configuration();
		conf.set("fs.hdfs.impl",
				org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
		conf.set("fs.file.impl",
				org.apache.hadoop.fs.LocalFileSystem.class.getName());

		FileSystem fs = inputdir.getFileSystem(conf);
		if(!fs.exists(input)) {
			System.err
					.println("HDFS input file doesnt exists");
			System.exit(1);
		}
		SequenceFile.Reader reader = new SequenceFile.Reader(fs, input, conf);

		List<String> randsets = DivideInstances.readLines(randpath, conf);

		PrintWriter pw1 = new PrintWriter(new FileWriter(new File(args[1] + "/set1")));
		PrintWriter pw2 = new PrintWriter(new FileWriter(new File(args[1] + "/set2")));

		Text key = new Text();
		VectorWritable obj = new VectorWritable();
		Integer j = new Integer(0);
		while (reader.next(key, obj)) {
			Vector vector = obj.get();
			Integer set = Integer.parseInt(randsets.get(j));
			PrintWriter pw = null;
			if(set == 0) pw = pw1;
			else pw = pw2;
			for (int i = 0; i < vector.size(); i++) {
				if (vector.get(i) > 0.1) {
					pw.print(i);
					pw.print(" ");
				}
			}
			pw.print("-1");
			pw.println();
			j += 1;
		}
		pw1.close();
		pw2.close();
	}
}
